import { Injectable } from '@angular/core';
import { BehaviorSubject, Observable } from 'rxjs';
import { Company } from './company.model';
import { HttpClient } from '@angular/common/http';
import { environment } from 'src/environments/environment';
import { AuthHttp } from 'src/app/shared/components/login/security/auth-http';

@Injectable({
  providedIn: 'root'
})
export class CompanyService {

  dataChange: BehaviorSubject<Company[]> = new BehaviorSubject<Company[]>([]);

  companyURL: string;

  constructor(private http: AuthHttp,
              private httpClient: HttpClient) {
    this.companyURL = `${environment.apiUrl}/company`;
  }

  findAll(): Observable<Company[]> {
    return this.http.get<Company[]>(`${this.companyURL}/all`);
  }

  findById(company: Company): any {
    return this.http.get(`${this.companyURL}/${company.id}`);
  }

  create(company: Company): any {
    return this.httpClient.post(this.companyURL, company);
  }

  update(company: Company): any {
    return this.http.put(`${this.companyURL}/${company.id}`, company);
  }

  deleteByOd(company: Company): any {
    return this.http.delete(`${this.companyURL}/${company.id}`);
  }

}
