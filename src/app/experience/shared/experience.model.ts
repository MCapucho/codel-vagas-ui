import { Candidate } from 'src/app/candidate/shared/candidate.model';

export class Experience {

  id?: number;
  startDate?: Date;
  endDate?: Date;
  company?: string;
  positionDescription?: string;
  candidate?: Candidate;
  position?: Position;

}
