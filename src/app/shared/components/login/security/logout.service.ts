import { Injectable } from '@angular/core';

import { AuthHttp } from './auth-http';
import { AuthService } from './auth.service';
import { environment } from 'src/environments/environment';

@Injectable({
  providedIn: 'root'
})

export class LogoutService {

  tokenRevokeURL: string;

  constructor(private http: AuthHttp,
              private authService: AuthService) {
                this.tokenRevokeURL = `${environment.apiUrl}/token/revoke`;
              }

  logout() {
    return this.http.delete(this.tokenRevokeURL, { withCredentials: true})
      .toPromise()
      .then(() => {
        this.authService.clearAccessToken();
      });
  }

}
