import { Candidate } from 'src/app/candidate/shared/candidate.model';

export class Competence {

  name?: string;
  candidates?: Candidate[];

}
