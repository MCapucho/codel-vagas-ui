import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';
import { NgModule } from '@angular/core';
import { RouterModule } from '@angular/router';

import { CoreModule } from '../core/core.module';

import { DebugComponent } from './debug/debug.component';
import { LoginComponent } from './login/login.component';

@NgModule({
  declarations: [
    DebugComponent,
    LoginComponent
  ],

  imports: [
    CommonModule,
    CoreModule,
    FormsModule,
    RouterModule
  ],

  exports: [
    DebugComponent,
    LoginComponent
  ],

  entryComponents: [
    LoginComponent
  ],

  providers: [
  ]
})
export class ComponentsModule { }
