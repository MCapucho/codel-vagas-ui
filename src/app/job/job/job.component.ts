import { Component, OnInit } from '@angular/core';
import { Job } from '../shared/job.model';

@Component({
  selector: 'app-job',
  templateUrl: './job.component.html',
  styleUrls: ['./job.component.css']
})
export class JobComponent implements OnInit {

  job: Job;

  constructor() { }

  ngOnInit() {
    this.job = {};
    this.job.id_position = {};
  }

  cancelJob() {

  }

  clearJob() {

  }

  saveJob() {

  }

}
