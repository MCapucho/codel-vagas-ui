import { JobCandidateDTO } from './job-candidate-dto.model';

export interface CandidateJobDTO {

  jobsList?: JobCandidateDTO[];

}
