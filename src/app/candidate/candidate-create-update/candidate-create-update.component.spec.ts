import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { CandidateCreateUpdateComponent } from './candidate-create-update.component';

describe('CandidateCreateUpdateComponent', () => {
  let component: CandidateCreateUpdateComponent;
  let fixture: ComponentFixture<CandidateCreateUpdateComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ CandidateCreateUpdateComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(CandidateCreateUpdateComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
