import { Component, OnInit } from '@angular/core';
import { MatSnackBar } from '@angular/material';
import { Router } from '@angular/router';

import { CompanyService } from '../shared/company.service';

import { Company } from '../shared/company.model';

@Component({
  selector: 'app-company-create-update',
  templateUrl: './company-create-update.component.html',
  styleUrls: ['./company-create-update.component.css']
})
export class CompanyCreateUpdateComponent implements OnInit {

  company: Company;

  step = 0;

  statesList = ['AC', 'AL', 'AP', 'AM', 'BA', 'CE', 'DF', 'ES', 'GO', 'MA', 'MT', 'MS', 'MG', 'PA', 'PB',
                'PR', 'PE', 'PI', 'RJ', 'RN', 'RS', 'RO', 'RR', 'SC', 'SP', 'SE', 'TO'];

  constructor(private companyService: CompanyService,
              private router: Router,
              public snackBar: MatSnackBar) { }

  ngOnInit() {
    this.company = {};
    this.company.address = {};
    this.company.userCompany = {};
  }

  setStep(index: number) {
    this.step = index;
  }

  nextStep() {
    this.step++;
  }

  previousStep() {
    this.step--;
  }

  cancelCompany() {
    this.router.navigate(['/home']);
  }

  clearCompany() {
    this.company = {
      id: null,
      fantasyName: null,
      cnpj: null,
      activityBranch: null,
      officeNumber: null,
      nameOfResponsible: null,
      responsibleNumber: null,
      address: {
        street: null,
        number: null,
        complement: null,
        zipCode: null,
        district: null,
        city: null,
        state: null
      },
      userCompany: {
        email: null,
        password: null
      }
    };
  }

  saveCompany() {
    this.company.userCompany.name = this.company.fantasyName;
    this.companyService.create(this.company).subscribe(data => {
      this.snackBar.open('Empresa cadastrada com sucesso', 'X', {
        duration: 3000
      });
      this.router.navigate(['/home']);
    });
  }

}
