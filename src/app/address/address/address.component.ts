import { Component, OnInit, AfterContentInit } from '@angular/core';

import { Address } from '../shared/address.model';
import { AddressService } from '../shared/address.service';

@Component({
  selector: 'app-address',
  templateUrl: './address.component.html',
  styleUrls: ['./address.component.css']
})
export class AddressComponent implements OnInit {

  address: Address;

  constructor(private addressService: AddressService) { }

  ngOnInit() {
    this.address = {};
  }

}
