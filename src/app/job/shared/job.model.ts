import { CandidateJob } from 'src/app/shared/models/candidate-job.model';
import { Company } from './../../company/shared/company.model';
import { Position } from 'src/app/position/shared/position.model';

export class Job {

  id?: number;
  title?: string;
  requirements?: string;
  differential?: string;
  jobType?: string;
  regime?: string;
  salary?: number;
  pcd?: boolean;
  status?: boolean;
  // tslint:disable-next-line: variable-name
  id_position?: Position;
  // tslint:disable-next-line: variable-name
  id_company?: Company;
  candidatesList?: CandidateJob[];

}
