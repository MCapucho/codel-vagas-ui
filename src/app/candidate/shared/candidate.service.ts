import { Injectable } from '@angular/core';
import { environment } from 'src/environments/environment';
import { Candidate } from './candidate.model';
import { Observable, BehaviorSubject } from 'rxjs';
import { AuthHttp } from 'src/app/shared/components/login/security/auth-http';

@Injectable({
  providedIn: 'root'
})
export class CandidateService {

  dataChange: BehaviorSubject<Candidate[]> = new BehaviorSubject<Candidate[]>([]);

  candidateURL: string;

  constructor(private http: AuthHttp) {
    this.candidateURL = `${environment.apiUrl}/candidate`;
  }

  findAll(): Observable<Candidate[]> {
    return this.http.get<Candidate[]>(`${this.candidateURL}/all`);
  }

  findById(company: Candidate): any {
    return this.http.get(`${this.candidateURL}/${company.id}`);
  }

  create(company: Candidate): any {
    return this.http.post(this.candidateURL, company);
  }

  update(company: Candidate): any {
    return this.http.put(`${this.candidateURL}/${company.id}`, company);
  }

  deleteByOd(company: Candidate): any {
    return this.http.delete(`${this.candidateURL}/${company.id}`);
  }

}
