import { Address } from './../../address/shared/address.model';
import { Competence } from 'src/app/competence/shared/competence.model';
import { Education } from 'src/app/education/shared/education.model';
import { Experience } from 'src/app/experience/shared/experience.model';
import { User } from 'src/app/user/shared/user.model';
import { CandidateJob } from 'src/app/shared/models/candidate-job.model';

export class Candidate {

  id?: number;
  name?: string;
  gender?: string;
  birthDate?: Date;
  cpf?: string;
  address?: Address;
  status?: boolean;
  phoneNumber?: string;
  mobileNumber?: string;
  pcd?: boolean;
  descriptionPcd?: string;
  cnh?: string;
  userCandidate?: User;
  educationList?: Education[];
  experienceList?: Experience[];
  competences?: Competence[];
  jobsList?: CandidateJob[]

}
