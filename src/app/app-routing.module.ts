
import { NgModule } from '@angular/core';
import { Routes, RouterModule, PreloadAllModules } from '@angular/router';

import { AuthGuard } from './shared/components/login/security/auth.guard';

import { NotAuthotizedComponent } from './shared/core/not-autthorized.component';
import { HomeComponent } from './home/home/home.component';

const routes: Routes = [
  {
    path: '',
    redirectTo: 'home',
    pathMatch: 'full'
  },

  {
    path: 'candidate',
    loadChildren: () => import('./candidate/candidate.module').then(m => m.CandidateModule)
  },

  {
    path: 'company',
    loadChildren: () => import('./company/company.module').then(m => m.CompanyModule)
  },

  {
    path: 'not-authorized',
    component: NotAuthotizedComponent
  },

];

@NgModule({
  imports: [
    RouterModule.forRoot(routes, {preloadingStrategy: PreloadAllModules})
  ],

  exports: [
    RouterModule
  ]
})
export class AppRoutingModule { }
