import { RouterModule, Routes } from '@angular/router';
import { NgModule } from '@angular/core';

import { CandidateCreateUpdateComponent } from './candidate-create-update/candidate-create-update.component';
import { CandidateListComponent } from './candidate-list/candidate-list.component';
import { AuthGuard } from '../shared/components/login/security/auth.guard';

const ROUTES: Routes = [
    {
        path: 'new',
        component: CandidateCreateUpdateComponent,
    },

    {
      path: 'home',
      component: CandidateListComponent,
      canActivate: [AuthGuard]
    }
];

@NgModule({
    imports: [RouterModule.forChild(ROUTES)],
    exports: [RouterModule]
})

export class CandidateRoutingModule { }
