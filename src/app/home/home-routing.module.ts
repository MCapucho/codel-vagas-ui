import { RouterModule, Routes } from '@angular/router';
import { NgModule } from '@angular/core';

import { HomeComponent } from './home/home.component';
import { AuthGuard } from '../shared/components/login/security/auth.guard';

const ROUTES: Routes = [
  {
      path: 'home',
      component: HomeComponent
  }
];

@NgModule({
    imports: [RouterModule.forChild(ROUTES)],
    exports: [RouterModule]
})
export class HomeRoutingModule { }
