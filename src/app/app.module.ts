import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { BrowserModule } from '@angular/platform-browser';
import { HTTP_INTERCEPTORS } from '@angular/common/http';
import { MatPaginatorIntl } from '@angular/material';
import { NgModule} from '@angular/core';

import { AppRoutingModule } from './app-routing.module';
import { ComponentsModule } from './shared/components/components.module';
import { CoreModule } from './shared/core/core.module';
import { HomeModule } from './home/home.module';
import { TemplateModule } from './shared/template/template.module';

import { AuthHttpInterceptor } from './shared/components/login/security/auth-http-interceptor';
import { AuthGuard } from './shared/components/login/security/auth.guard';
import { JwtModule, JwtHelperService } from '@auth0/angular-jwt';
import { LogoutService } from './shared/components/login/security/logout.service';

import { AppComponent } from './app.component';
import { environment } from 'src/environments/environment';
import { PortuguesPaginator } from './shared/components/models/portugues-paginator.model';

export function tokenGetter(): string {
  return localStorage.getItem('token');
}

@NgModule({
  declarations: [
    AppComponent
  ],

  imports: [
    AppRoutingModule,
    BrowserAnimationsModule,
    BrowserModule,
    CoreModule,
    ComponentsModule,
    HomeModule,
    TemplateModule,

    JwtModule.forRoot({
      config: {
        // tslint:disable-next-line: object-literal-shorthand
        tokenGetter: tokenGetter,
        whitelistedDomains: environment.tokenWhitelistedDomains,
        blacklistedRoutes: environment.tokenBlacklistedRoutes
      }
    })
  ],

  providers: [
    { provide: MatPaginatorIntl, useClass: PortuguesPaginator },
    {
      provide: HTTP_INTERCEPTORS,
      useClass: AuthHttpInterceptor,
      multi: true
    },
    AuthGuard,
    JwtHelperService,
    LogoutService
  ],

  bootstrap: [AppComponent]
})
export class AppModule { }
