import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';

import { ComponentsModule } from '../shared/components/components.module';
import { CoreModule } from '../shared/core/core.module';
import { FormsModule } from '@angular/forms';

import { JobComponent } from './job/job.component';

@NgModule({
  declarations: [
    JobComponent
  ],

  imports: [
    CommonModule,
    ComponentsModule,
    CoreModule,
    FormsModule
  ],

  exports: [
    JobComponent
  ]
})
export class JobModule { }
