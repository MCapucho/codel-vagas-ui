import { Address } from 'src/app/address/shared/address.model';
import { User } from 'src/app/user/shared/user.model';

export interface CompanyDTO {

  id?: number;
  fantasyName?: string;
  cnpj?: string;
  address?: Address;
  nameOfResponsible?: string;
  officeNumber?: string;
  responsibleNumber?: string;
  activityBranch?: string;
  userCompany?: User;
  jobs?: number[];

}
