import { Candidate } from 'src/app/candidate/shared/candidate.model';
import { Company } from 'src/app/company/shared/company.model';
import { Permission } from 'src/app/shared/models/permission.model';

export class User {

  id?: number;
  name?: string;
  email?: string;
  password?: string;
  candidate?: Candidate;
  company?: Company;
  permissions?: Permission[];

}
