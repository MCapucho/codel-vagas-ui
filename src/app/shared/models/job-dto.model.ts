export interface JobDTO {

  id?: number;
  title?: string;
  requirements?: string;
  differential?: string;
  jobType?: string;
  regime?: string;
  salary?: number;
  pcd?: boolean;
  status?: boolean;
  position?: number;
  idCompany?: number;
  nameCompany?: string;
  candidates?: number[];

}
