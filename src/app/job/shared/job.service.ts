import { HttpParams, HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';

import { AuthHttp } from 'src/app/shared/components/login/security/auth-http';
import { environment } from 'src/environments/environment';
import { Job } from './job.model';
import { Observable, Subject } from 'rxjs';
import { Paginated } from 'src/app/shared/components/models/paginated.model';

@Injectable({
  providedIn: 'root'
})
export class JobService {

  jobURL: string;
  jobsSubject: Subject<Job[]> = new Subject<Job[]>();

  constructor(private http: HttpClient) {
    this.jobURL = `${environment.apiUrl}/job`;
  }

  findByPageJobsStatusTrue(page: number): Observable<Paginated<Job>> {
    const params = new HttpParams()
      .set('direction', 'asc')
      .set('linesPerPage', '6')
      .set('orderBy', 'title')
      .set('page', page.toString());

    return this.http.get<Paginated<Job>>(`${this.jobURL}/status/true/page`, { params });
  }

  search(title: string): Observable<Job[]> {
    const params = new HttpParams()
      .set('title', title);

    return this.http.get<Job[]>(`${this.jobURL}/search`, { params });
  }

}
