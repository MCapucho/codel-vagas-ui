import { Injectable } from '@angular/core';
import { Address } from './address.model';

@Injectable({
  providedIn: 'root'
})
export class AddressService {

  address;

  constructor() { }

  pegarEndereco(address: Address) {
    this.address =  address;
  }

}
