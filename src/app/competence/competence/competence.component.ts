import { COMMA, ENTER } from '@angular/cdk/keycodes';
import { Component, OnInit, ViewChild, ElementRef } from '@angular/core';
import { Competence } from '../shared/competence.model';
import { FormControl } from '@angular/forms';
import { Observable } from 'rxjs';
import { MatAutocomplete, MatChipInputEvent, MatAutocompleteSelectedEvent } from '@angular/material';
import { map, startWith } from 'rxjs/operators';

const competenceMock: Competence[] = [
  {
    name: 'Teste 1'
  },

  {
    name: 'Teste2'
  },

  {
    name: 'Teste 3'
  },

  {
    name: 'Teste 4'
  },
];

@Component({
  selector: 'app-competence',
  templateUrl: './competence.component.html',
  styleUrls: ['./competence.component.css']
})
export class CompetenceComponent implements OnInit {

  visible = true;
  selectable = true;
  removable = true;
  separatorKeysCodes: number[] = [ENTER, COMMA];
  competenceControl = new FormControl();
  filteredCompetences: Observable<any[]>;
  competences: string[] = [];
  allCompetences: Competence[] = competenceMock;
  teste: Competence;

  @ViewChild('competenceInput', {static: true}) competenceInput: ElementRef<HTMLInputElement>;
  @ViewChild('auto', {static: true}) matAutocomplete: MatAutocomplete;

  constructor() {
    this.filteredCompetences = this.competenceControl.valueChanges.pipe(
      startWith(null),
      map((competence) => competence ? this._filter(competence.name) : this.allCompetences.slice()));
  }

  ngOnInit() {
  }

  add(event: MatChipInputEvent): void {
    const input = event.input;
    const value = event.value;

    if ((value || '').trim()) {
      this.competences.push(value.trim());
    }

    if (input) {
      input.value = '';
    }

    this.competenceControl.setValue(null);
  }

  remove(competence: string): void {
    const index = this.competences.indexOf(competence);

    if (index >= 0) {
      this.competences.splice(index, 1);
    }
  }

  selected(event: MatAutocompleteSelectedEvent): void {
    this.competences.push(event.option.viewValue);
    this.competenceInput.nativeElement.value = '';
    this.competenceControl.setValue(null);
  }

  private _filter(value: string): Competence[] {
    const filterValue = value.toLowerCase();

    return this.allCompetences.filter(competence => competence.name.toLowerCase().indexOf(filterValue) === 0);
  }

}
