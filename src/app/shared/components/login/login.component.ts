import { Component, OnInit, Input, Output, EventEmitter } from '@angular/core';
import { MatDialog, MatSnackBar } from '@angular/material';
import { Router } from '@angular/router';

import { AuthService } from './security/auth.service';
import { ErrorHandlerService } from '../../core/error-handler.service';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.css']
})
export class LoginComponent implements OnInit {

  @Input() textTitle = '';
  @Input() textFooter = '';
  @Input() textRouter = '';
  @Input() type = '';

  constructor(private authService: AuthService,
              private errorHandler: ErrorHandlerService,
              private router: Router,
              public dialog: MatDialog,
              public snackBar: MatSnackBar) { }

  ngOnInit() {
  }

  login(email: string, password: string) {
    this.authService.login(email, password)
      .then(() => {
        if (this.type === 'company') {
          if (this.authService.jwtPayload.isCandidate === false) {
            this.router.navigate(['/company/home']);
            this.closeDialog();
          } else {
            this.authService.clearAccessToken();
            this.snackBar.open('Entrar no login de candidato', 'X', {
              duration: 3000
            });
          }
        } else {
          if (this.authService.jwtPayload.isCandidate === true) {
            this.router.navigate(['/candidate/home']);
            this.closeDialog();
          } else {
            this.authService.clearAccessToken();
            this.snackBar.open('Entrar no login de empresa', 'X', {
              duration: 3000
            });
          }
        }
      })
      .catch(err => {
        this.errorHandler.handle(err);
      });
  }

  closeDialog() {
    this.dialog.closeAll();
  }

}
