import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';
import { FormsModule } from '@angular/forms';

import { ComponentsModule } from '../shared/components/components.module';
import { CoreModule } from '../shared/core/core.module';

import { HomeComponent } from './home/home.component';

import { HomeRoutingModule } from './home-routing.module';

@NgModule({
  declarations: [
    HomeComponent
  ],

  imports: [
    CommonModule,
    ComponentsModule,
    CoreModule,
    FormsModule,

    HomeRoutingModule
  ],

  exports: [
    HomeComponent
  ]
})

export class HomeModule { }
