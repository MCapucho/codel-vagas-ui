import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { AddressModule } from '../address/address.module';
import { CompetenceModule } from '../competence/competence.module';
import { ComponentsModule } from '../shared/components/components.module';
import { CoreModule } from '../shared/core/core.module';

import { CandidateRoutingModule } from './candidate-routing.module';

import { CandidateCreateUpdateComponent } from './candidate-create-update/candidate-create-update.component';
import { CandidateDetailsComponent } from './candidate-details/candidate-details.component';
import { CandidateListComponent } from './candidate-list/candidate-list.component';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';

@NgModule({
  declarations: [
    CandidateCreateUpdateComponent,
    CandidateDetailsComponent,
    CandidateListComponent
  ],

  imports: [
    AddressModule,
    CommonModule,
    CompetenceModule,
    ComponentsModule,
    CoreModule,
    FormsModule,
    ReactiveFormsModule,

    CandidateRoutingModule
  ],

  exports: [
    CandidateCreateUpdateComponent,
    CandidateDetailsComponent,
    CandidateListComponent
  ]
})

export class CandidateModule { }
