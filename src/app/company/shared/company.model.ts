import { Address } from './../../address/shared/address.model';
import { User } from 'src/app/user/shared/user.model';
import { Job } from 'src/app/job/shared/job.model';

export class Company {

  id?: number;
  fantasyName?: string;
  cnpj?: string;
  address?: Address;
  nameOfResponsible?: string;
  officeNumber?: string;
  responsibleNumber?: string;
  activityBranch?: string;
  userCompany?: User;
  jobsList?: Job;

}
