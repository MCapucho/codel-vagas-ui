import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';

import { CandidateService } from '../shared/candidate.service';

import { Candidate } from '../shared/candidate.model';

@Component({
  selector: 'app-candidate-create-update',
  templateUrl: './candidate-create-update.component.html',
  styleUrls: ['./candidate-create-update.component.css']
})
export class CandidateCreateUpdateComponent implements OnInit {

  candidate: Candidate;

  statesList = ['AC', 'AL', 'AP', 'AM', 'BA', 'CE', 'DF', 'ES', 'GO', 'MA', 'MT', 'MS', 'MG', 'PA', 'PB',
                'PB', 'PB', 'PR', 'PE', 'PI', 'RJ', 'RN', 'RS', 'RO', 'RR', 'SC', 'SP', 'SE', 'TO'];

  constructor(private candidateService: CandidateService,
              private router: Router) { }

  ngOnInit() { }

  cancelCandidate() {
    this.router.navigate(['/home']);
  }

  clearCandidate() {
    this.candidate = {
      id: null,
      name: null,
      cpf: null,
      gender: null,
      birthDate: null,
      status: null,
      phoneNumber: null,
      mobileNumber: null,
      pcd: null,
      descriptionPcd: null,
      cnh: null,
      address: {
        street: null,
        number: null,
        complement: null,
        zipCode: null,
        district: null,
        city: null,
        state: null
      },
      userCandidate: {
        email: null,
        password: null
      }
    };
  }

  saveCandidate() {
    this.candidate.userCandidate.name = this.candidate.name;
    this.candidateService.create(this.candidate).subscribe(data => {
      this.router.navigate(['/home']);
    });
  }

}
