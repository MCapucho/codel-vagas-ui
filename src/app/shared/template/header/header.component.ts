import { Component, OnInit } from '@angular/core';
import { MatDialog } from '@angular/material';
import { Router } from '@angular/router';

import { AuthService } from '../../components/login/security/auth.service';
import { ErrorHandlerService } from '../../core/error-handler.service';
import { LogoutService } from '../../components/login/security/logout.service';

import { LoginComponent } from '../../components/login/login.component';

@Component({
  selector: 'app-header',
  templateUrl: './header.component.html',
  styleUrls: ['./header.component.css']
})
export class HeaderComponent implements OnInit {

  constructor(private authService: AuthService,
              private errorHandler: ErrorHandlerService,
              private logoutService: LogoutService,
              private router: Router,
              public dialog: MatDialog) { }

  ngOnInit() { }

  get isLogged(): boolean {
    return !this.authService.isAccessTokenInvalid();
  }

  get isCandidate(): boolean {
    if (this.authService.jwtPayload !== null) {
      return this.authService.jwtPayload.isCandidate;
    }

    return false;
  }

  openCompany(): void {
    const dialogRef = this.dialog.open(LoginComponent, {
      disableClose: true,
      width: '600px'
    });

    dialogRef.componentInstance.textTitle = 'Acesso Empresa';
    dialogRef.componentInstance.textFooter = 'Nova Empresa';
    dialogRef.componentInstance.textRouter = '/company/new';
    dialogRef.componentInstance.type = 'company';
  }

  openCandidate(): void {
    const dialogRef = this.dialog.open(LoginComponent, {
      disableClose: true,
      width: '600px'
    });

    dialogRef.componentInstance.textTitle = 'Acesso Candidato';
    dialogRef.componentInstance.textFooter = 'Novo Candidato';
    dialogRef.componentInstance.textRouter = '/candidate/new';
    dialogRef.componentInstance.type = 'candidate';
  }

  logout() {
    this.logoutService.logout()
      .then(() => {
        this.router.navigate(['/home']);
      })
      .catch(erro => {
        this.errorHandler.handle(erro);
      });
  }

}
