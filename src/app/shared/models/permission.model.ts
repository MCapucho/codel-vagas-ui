import { User } from 'src/app/user/shared/user.model';

export class Permission {

  id?: number;
  description?: string;
  users?: User[];

}
