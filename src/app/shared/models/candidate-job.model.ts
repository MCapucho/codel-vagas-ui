import { CandidateJobKey } from './candidate-job-key.model';
import { Candidate } from 'src/app/candidate/shared/candidate.model';
import { Job } from 'src/app/job/shared/job.model';

export interface CandidateJob {

  id?: CandidateJobKey;
  candidate?: Candidate;
  job?: Job;
  sendEmail?: boolean;
  score?: number;
  readed?: boolean;

}
