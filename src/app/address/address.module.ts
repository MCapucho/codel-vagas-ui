import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';

import { CoreModule } from '../shared/core/core.module';
import { ComponentsModule } from '../shared/components/components.module';

import { AddressComponent } from './address/address.component';

@NgModule({
  declarations: [
    AddressComponent
  ],

  imports: [
    CommonModule,
    ComponentsModule,
    CoreModule,
    FormsModule
  ],

  exports: [
    AddressComponent
  ]
})

export class AddressModule { }
