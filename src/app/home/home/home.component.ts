import { Component, OnInit, ViewChild, LOCALE_ID } from '@angular/core';
import { MatPaginator, PageEvent, MAT_DATE_LOCALE, DateAdapter, MAT_DATE_FORMATS } from '@angular/material';
import { MomentDateAdapter, MAT_MOMENT_DATE_FORMATS } from '@angular/material-moment-adapter';

import { registerLocaleData } from '@angular/common';
import localept from '@angular/common/locales/pt';

registerLocaleData(localept, 'pt');

import { JobService } from 'src/app/job/shared/job.service';

import { Job } from 'src/app/job/shared/job.model';
import { Paginated } from 'src/app/shared/components/models/paginated.model';
import { Subscription } from 'rxjs';

@Component({
  selector: 'app-home',
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.css'],
  providers: [
    { provide: MAT_DATE_LOCALE, useValue: 'pt-BR' },
    { provide: LOCALE_ID, useValue: 'pt'},
    { provide: DateAdapter, useClass: MomentDateAdapter, deps: [MAT_DATE_LOCALE] },
    { provide: MAT_DATE_FORMATS, useValue: MAT_MOMENT_DATE_FORMATS }
  ]
})
export class HomeComponent implements OnInit {

  @ViewChild(MatPaginator, {static: true}) paginator: MatPaginator;

  paginated: Paginated<Job>;
  direction: string;
  orderBy: string;
  paginatorLength: number;

  jobs: Job[];
  jobsSubs = new Subscription();
  searchVar = '';

  constructor(private jobService: JobService) { }

  ngOnInit() {
    this.jobsSubs = this.jobService.jobsSubject.subscribe(data => {
      this.jobs = data;
    });

    this.chargeJobs(0);
  }

  chargeJobs(index: number) {
    this.jobService.findByPageJobsStatusTrue(index).subscribe(resp => {
      this.jobs = resp.content;
      this.paginated = resp;
    });
  }

  changePage(page: PageEvent) {
    this.chargeJobs(page.pageIndex);
  }

  search(event) {
    if (this.searchVar.length >= 3 ) {
      this.jobService.search(this.searchVar).subscribe(resp => {
        this.jobService.jobsSubject.next(resp);
      }, err => {
        console.log(err);
      });
    } else {
      this.chargeJobs(0);
    }
  }

}
