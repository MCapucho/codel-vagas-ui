import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { PositionComponent } from './position/position.component';



@NgModule({
  declarations: [PositionComponent],
  imports: [
    CommonModule
  ]
})
export class PositionModule { }
