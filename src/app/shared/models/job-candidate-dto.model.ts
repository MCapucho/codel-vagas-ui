export interface JobCandidateDTO {

  id?: number;
  title?: string;
  requirements?: string;
  salary?: number;
  candidates?: number;
  nameCompany?: string;
  status?: boolean;

}
