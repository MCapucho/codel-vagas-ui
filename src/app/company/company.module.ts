import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { AddressModule } from '../address/address.module';
import { ComponentsModule } from '../shared/components/components.module';
import { CoreModule } from '../shared/core/core.module';

import { CompanyRoutingModule } from './company-routing.module';

import { CompanyCreateUpdateComponent } from './company-create-update/company-create-update.component';
import { CompanyDetailsComponent } from './company-details/company-details.component';
import { CompanyListComponent } from './company-list/company-list.component';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { UserModule } from '../user/user.module';
import { JobModule } from '../job/job.module';

@NgModule({
  declarations: [
    CompanyCreateUpdateComponent,
    CompanyListComponent,
    CompanyDetailsComponent
  ],

  imports: [
    AddressModule,
    CommonModule,
    ComponentsModule,
    CoreModule,
    FormsModule,
    JobModule,
    ReactiveFormsModule,
    UserModule,

    CompanyRoutingModule
  ],

  exports: [
    CompanyCreateUpdateComponent,
    CompanyListComponent,
    CompanyDetailsComponent
  ]
})

export class CompanyModule { }
