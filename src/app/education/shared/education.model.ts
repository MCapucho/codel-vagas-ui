import { Candidate } from 'src/app/candidate/shared/candidate.model';

export class Education {

  id?: number;
  startDate?: Date;
  endDate?: Date;
  formation?: string;
  institution?: string;
  course?: string;
  candidate?: Candidate;

}
