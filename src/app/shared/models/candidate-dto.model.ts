import { Address } from 'src/app/address/shared/address.model';
import { Competence } from 'src/app/competence/shared/competence.model';
import { Education } from 'src/app/education/shared/education.model';
import { Experience } from 'src/app/experience/shared/experience.model';
import { User } from 'src/app/user/shared/user.model';

export interface CandidateDTO {

  id?: number;
  gender?: string;
  birthDate?: Date;
  cpf?: string;
  address?: Address;
  status?: boolean;
  phoneNumber?: string;
  mobileNumber?: string;
  pcd?: boolean;
  descriptionPcd?: string;
  cnh?: string;
  userCandidate?: User;
  educationList?: Education;
  experienceList?: Experience;
  competences?: Competence;
  jobs?: number[];

}
