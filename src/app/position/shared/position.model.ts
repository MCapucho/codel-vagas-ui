import { Experience } from 'src/app/experience/shared/experience.model';
import { Job } from 'src/app/job/shared/job.model';

export class Position {

  id?: number;
  name?: string;
  area?: string;
  jobList?: Job[];
  experienceList?: Experience[];

}
