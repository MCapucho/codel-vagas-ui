import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { CoreModule } from '../shared/core/core.module';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';

import { CompetenceComponent } from './competence/competence.component';

@NgModule({
  declarations: [
    CompetenceComponent
  ],

  imports: [
    CommonModule,
    CoreModule,
    FormsModule,
    ReactiveFormsModule
  ],

  exports: [
    CompetenceComponent
  ]
})
export class CompetenceModule { }
