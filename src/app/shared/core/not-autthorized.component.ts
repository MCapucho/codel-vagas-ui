import { Component, OnInit } from '@angular/core';

@Component({
  template: `
  <div class="mcr-container">
    <h1 class="text-center">Acesso negado!</h1>
  </div>
  `,
})

export class NotAuthotizedComponent implements OnInit {

  constructor() { }

  ngOnInit() {
  }

}

