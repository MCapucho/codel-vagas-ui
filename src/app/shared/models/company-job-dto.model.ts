import { JobCandidateDTO } from './job-candidate-dto.model';

export interface CompanyJobDTO {

  jobsList?: JobCandidateDTO[];

}
