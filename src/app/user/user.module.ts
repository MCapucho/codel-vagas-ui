import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';
import { NgModule } from '@angular/core';

import { ComponentsModule } from '../shared/components/components.module';
import { CoreModule } from '../shared/core/core.module';

import { UserComponent } from './user/user.component';

@NgModule({
  declarations: [
    UserComponent
  ],

  imports: [
    CommonModule,
    ComponentsModule,
    CoreModule,
    FormsModule
  ],

  exports: [
    UserComponent
  ]
})
export class UserModule { }
