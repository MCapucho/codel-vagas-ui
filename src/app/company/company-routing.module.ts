import { RouterModule, Routes } from '@angular/router';
import { NgModule } from '@angular/core';

import { AuthGuard } from '../shared/components/login/security/auth.guard';
import { CompanyCreateUpdateComponent } from './company-create-update/company-create-update.component';
import { CompanyListComponent } from './company-list/company-list.component';
import { JobComponent } from '../job/job/job.component';

const ROUTES: Routes = [
    {
      path: 'new',
      component: CompanyCreateUpdateComponent
    },

    {
      path: 'home',
      component: CompanyListComponent,
      canActivate: [AuthGuard]
    },

    {
      path: 'job/new',
      component: JobComponent,
      canActivate: [AuthGuard]
    },

    {
      path: 'edit',
      component: CompanyCreateUpdateComponent,
      canActivate: [AuthGuard]
    }
];

@NgModule({
    imports: [RouterModule.forChild(ROUTES)],
    exports: [RouterModule]
})

export class CompanyRoutingModule { }
